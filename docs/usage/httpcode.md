---
layout: doc
---

<script setup>
const args = {
    code: [],
};
</script>

##### laylatichy\nano\core\httpcode\HttpCode

## usage

`HttpCode` is an enum class that contains the available http status codes

```php
use laylatichy\nano\core\httpcode\HttpCode;

HttpCode::I_AM_A_TEAPOT
```

## <Types fn="code" r="int" :args="args.code" /> {#code}

```php
HttpCode::I_AM_A_TEAPOT->code(); // 418
```

