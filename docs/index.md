---
layout: home

hero:
    name:    nano/core/httpcode
    tagline: http status codes for nano
    actions:
        -   theme: brand
            text:  get started
            link:  /getting-started/introduction

features:
    -   title:   simple and minimal, always
        details: |
                 nano is a simple and minimal framework, and so are its modules. nano/core/httpcode is a http status codes module for nano
    -   title:   http status codes
        details: |
                 nano/core/httpcode provides a simple way to use http status codes
---
