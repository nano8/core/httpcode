import { defineConfig } from 'vitepress';

export default defineConfig({
    lastUpdated:   true,
    sitemap:       {
        hostname: 'https://nano8.gitlab.io/core/httpcode/',
    },
    title:         'nano/core/httpcode',
    titleTemplate: 'nano/core/httpcode - documentation',
    description:   'http status codes for laylatichy/nano framework',
    base:          '/core/httpcode/',
    themeConfig:   {
        lastUpdated: {
            text:          'updated',
            formatOptions: {
                dateStyle: 'long',
                timeStyle: 'medium',
            },
        },
        search:      {
            provider: 'local',
        },
        nav:         [],
        sidebar:     [
            {
                text:  'getting started',
                items: [
                    {
                        text: 'introduction',
                        link: '/getting-started/introduction',
                    },
                    {
                        text: 'installation',
                        link: '/getting-started/installation',
                    },
                ],
            },
            {
                text:  'usage',
                items: [
                    {
                        text: 'HttpCode',
                        link: '/usage/httpcode',
                    },
                ],
            },
            {
                text: 'nano documentation',
                link: 'https://nano.laylatichy.com',
            },
        ],
        socialLinks: [
            {
                icon: 'github',
                link: 'https://gitlab.com/nano8/core/httpcode',
            },
        ],
        editLink:    {
            text:    'edit this page',
            pattern: 'https://gitlab.com/nano8/core/httpcode/-/edit/dev/docs/:path',
        },
    },
});
