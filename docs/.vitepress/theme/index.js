import DefaultTheme from 'vitepress/theme';
import Types        from '../../components/types.vue';
import './custom.css';

export default {
    extends: DefaultTheme,
    enhanceApp(ctx) {
        ctx.app.component('Types', Types);
    },
};