<?php

namespace laylatichy\nano\core\httpcode;

use InvalidArgumentException;

enum HttpCode {
    case SWITCHING_PROTOCOLS;
    case PROCESSING; // RFC2518
    case OK;
    case CREATED;
    case ACCEPTED;
    case NON_AUTHORITATIVE_INFORMATION;
    case NO_CONTENT;
    case RESET_CONTENT;
    case PARTIAL_CONTENT;
    case MULTI_STATUS;     // RFC4918
    case ALREADY_REPORTED; // RFC5842
    case IM_USED;          // RFC3229
    case MULTIPLE_CHOICES;
    case MOVED_PERMANENTLY;
    case FOUND;
    case SEE_OTHER;
    case NOT_MODIFIED;
    case USE_PROXY;
    case RESERVED;
    case TEMPORARY_REDIRECT;
    case PERMANENTLY_REDIRECT; // RFC7238
    case BAD_REQUEST;
    case UNAUTHORIZED;
    case PAYMENT_REQUIRED;
    case FORBIDDEN;
    case NOT_FOUND;
    case METHOD_NOT_ALLOWED;
    case NOT_ACCEPTABLE;
    case PROXY_AUTHENTICATION_REQUIRED;
    case REQUEST_TIMEOUT;
    case CONFLICT;
    case GONE;
    case LENGTH_REQUIRED;
    case PRECONDITION_FAILED;
    case REQUEST_ENTITY_TOO_LARGE;
    case REQUEST_URI_TOO_LONG;
    case UNSUPPORTED_MEDIA_TYPE;
    case REQUESTED_RANGE_NOT_SATISFIABLE;
    case EXPECTATION_FAILED;
    case I_AM_A_TEAPOT;                                             // RFC2324
    case UNPROCESSABLE_ENTITY;                                      // RFC4918
    case LOCKED;                                                    // RFC4918
    case FAILED_DEPENDENCY;                                         // RFC4918
    case RESERVED_FOR_WEBDAV_ADVANCED_COLLECTIONS_EXPIRED_PROPOSAL; // RFC2817
    case UPGRADE_REQUIRED;                                          // RFC2817
    case PRECONDITION_REQUIRED;                                     // RFC6585
    case TOO_MANY_REQUESTS;                                         // RFC6585
    case REQUEST_HEADER_FIELDS_TOO_LARGE;                           // RFC6585
    case INTERNAL_SERVER_ERROR;
    case NOT_IMPLEMENTED;
    case BAD_GATEWAY;
    case SERVICE_UNAVAILABLE;
    case GATEWAY_TIMEOUT;
    case VERSION_NOT_SUPPORTED;
    case VARIANT_ALSO_NEGOTIATES_EXPERIMENTAL; // RFC2295
    case INSUFFICIENT_STORAGE;                 // RFC4918
    case LOOP_DETECTED;                        // RFC5842
    case NOT_EXTENDED;                         // RFC2774
    case NETWORK_AUTHENTICATION_REQUIRED;      // RFC6585

    public function code(): int {
        return match ($this) {
            self::SWITCHING_PROTOCOLS                                       => 101,
            self::PROCESSING                                                => 102,
            self::OK                                                        => 200,
            self::CREATED                                                   => 201,
            self::ACCEPTED                                                  => 202,
            self::NON_AUTHORITATIVE_INFORMATION                             => 203,
            self::NO_CONTENT                                                => 204,
            self::RESET_CONTENT                                             => 205,
            self::PARTIAL_CONTENT                                           => 206,
            self::MULTI_STATUS                                              => 207,
            self::ALREADY_REPORTED                                          => 208,
            self::IM_USED                                                   => 226,
            self::MULTIPLE_CHOICES                                          => 300,
            self::MOVED_PERMANENTLY                                         => 301,
            self::FOUND                                                     => 302,
            self::SEE_OTHER                                                 => 303,
            self::NOT_MODIFIED                                              => 304,
            self::USE_PROXY                                                 => 305,
            self::RESERVED                                                  => 306,
            self::TEMPORARY_REDIRECT                                        => 307,
            self::PERMANENTLY_REDIRECT                                      => 308,
            self::BAD_REQUEST                                               => 400,
            self::UNAUTHORIZED                                              => 401,
            self::PAYMENT_REQUIRED                                          => 402,
            self::FORBIDDEN                                                 => 403,
            self::NOT_FOUND                                                 => 404,
            self::METHOD_NOT_ALLOWED                                        => 405,
            self::NOT_ACCEPTABLE                                            => 406,
            self::PROXY_AUTHENTICATION_REQUIRED                             => 407,
            self::REQUEST_TIMEOUT                                           => 408,
            self::CONFLICT                                                  => 409,
            self::GONE                                                      => 410,
            self::LENGTH_REQUIRED                                           => 411,
            self::PRECONDITION_FAILED                                       => 412,
            self::REQUEST_ENTITY_TOO_LARGE                                  => 413,
            self::REQUEST_URI_TOO_LONG                                      => 414,
            self::UNSUPPORTED_MEDIA_TYPE                                    => 415,
            self::REQUESTED_RANGE_NOT_SATISFIABLE                           => 416,
            self::EXPECTATION_FAILED                                        => 417,
            self::I_AM_A_TEAPOT                                             => 418,
            self::UNPROCESSABLE_ENTITY                                      => 422,
            self::LOCKED                                                    => 423,
            self::FAILED_DEPENDENCY                                         => 424,
            self::RESERVED_FOR_WEBDAV_ADVANCED_COLLECTIONS_EXPIRED_PROPOSAL => 425,
            self::UPGRADE_REQUIRED                                          => 426,
            self::PRECONDITION_REQUIRED                                     => 428,
            self::TOO_MANY_REQUESTS                                         => 429,
            self::REQUEST_HEADER_FIELDS_TOO_LARGE                           => 431,
            self::INTERNAL_SERVER_ERROR                                     => 500,
            self::NOT_IMPLEMENTED                                           => 501,
            self::BAD_GATEWAY                                               => 502,
            self::SERVICE_UNAVAILABLE                                       => 503,
            self::GATEWAY_TIMEOUT                                           => 504,
            self::VERSION_NOT_SUPPORTED                                     => 505,
            self::VARIANT_ALSO_NEGOTIATES_EXPERIMENTAL                      => 506,
            self::INSUFFICIENT_STORAGE                                      => 507,
            self::LOOP_DETECTED                                             => 508,
            self::NOT_EXTENDED                                              => 510,
            self::NETWORK_AUTHENTICATION_REQUIRED                           => 511,
        };
    }

    public static function from(int $code): self {
        return match ($code) {
            101     => self::SWITCHING_PROTOCOLS,
            102     => self::PROCESSING,
            200     => self::OK,
            201     => self::CREATED,
            202     => self::ACCEPTED,
            203     => self::NON_AUTHORITATIVE_INFORMATION,
            204     => self::NO_CONTENT,
            205     => self::RESET_CONTENT,
            206     => self::PARTIAL_CONTENT,
            207     => self::MULTI_STATUS,
            208     => self::ALREADY_REPORTED,
            226     => self::IM_USED,
            300     => self::MULTIPLE_CHOICES,
            301     => self::MOVED_PERMANENTLY,
            302     => self::FOUND,
            303     => self::SEE_OTHER,
            304     => self::NOT_MODIFIED,
            305     => self::USE_PROXY,
            306     => self::RESERVED,
            307     => self::TEMPORARY_REDIRECT,
            308     => self::PERMANENTLY_REDIRECT,
            400     => self::BAD_REQUEST,
            401     => self::UNAUTHORIZED,
            402     => self::PAYMENT_REQUIRED,
            403     => self::FORBIDDEN,
            404     => self::NOT_FOUND,
            405     => self::METHOD_NOT_ALLOWED,
            406     => self::NOT_ACCEPTABLE,
            407     => self::PROXY_AUTHENTICATION_REQUIRED,
            408     => self::REQUEST_TIMEOUT,
            409     => self::CONFLICT,
            410     => self::GONE,
            411     => self::LENGTH_REQUIRED,
            412     => self::PRECONDITION_FAILED,
            413     => self::REQUEST_ENTITY_TOO_LARGE,
            414     => self::REQUEST_URI_TOO_LONG,
            415     => self::UNSUPPORTED_MEDIA_TYPE,
            416     => self::REQUESTED_RANGE_NOT_SATISFIABLE,
            417     => self::EXPECTATION_FAILED,
            418     => self::I_AM_A_TEAPOT,
            422     => self::UNPROCESSABLE_ENTITY,
            423     => self::LOCKED,
            424     => self::FAILED_DEPENDENCY,
            425     => self::RESERVED_FOR_WEBDAV_ADVANCED_COLLECTIONS_EXPIRED_PROPOSAL,
            426     => self::UPGRADE_REQUIRED,
            428     => self::PRECONDITION_REQUIRED,
            429     => self::TOO_MANY_REQUESTS,
            431     => self::REQUEST_HEADER_FIELDS_TOO_LARGE,
            500     => self::INTERNAL_SERVER_ERROR,
            501     => self::NOT_IMPLEMENTED,
            502     => self::BAD_GATEWAY,
            503     => self::SERVICE_UNAVAILABLE,
            504     => self::GATEWAY_TIMEOUT,
            505     => self::VERSION_NOT_SUPPORTED,
            506     => self::VARIANT_ALSO_NEGOTIATES_EXPERIMENTAL,
            507     => self::INSUFFICIENT_STORAGE,
            508     => self::LOOP_DETECTED,
            510     => self::NOT_EXTENDED,
            511     => self::NETWORK_AUTHENTICATION_REQUIRED,
            default => throw new InvalidArgumentException('invalid HttpCode'),
        };
    }
}
